provider "aws" {
    region = "us-west-2"
    profile = "stelligentmfa"
}

resource "aws_instance" "trevor" {
    associate_public_ip_address = true
    ami = "ami-06d51e91cea0dac8d"
    subnet_id = "${aws_subnet.public.id}"
    instance_type = "t3.micro"
    tags = {
        Name = "Trevor"
    }
}

resource "aws_vpc" "trevor" {
    cidr_block = "10.0.0.0/16"
}

resource "aws_internet_gateway" "gateway" {
    vpc_id = "${aws_vpc.trevor.id}"
}

resource "aws_subnet" "public" {
    vpc_id = "${aws_vpc.trevor.id}"
    cidr_block = "10.0.0.0/24"
}

resource "aws_key_pair" "deployer" {
  key_name   = "trevor"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC5DSl8pDxICnqFBRsKKMLF1sA+9FgXDpSgZvBh3mNXmGRT3gI4SpHvFbZSmFDjJS6SbcjW6XDeFrBKuUlxP1u/MR4HaqK2EvK3sBEIVtg5GCwzCJdXoDMaT1bU1TSc03fJhK9SY63dRK/zHup1uOiBcPq6zJr28gJ34NL+TrxYEKUck7TWMzrY9TVjJLvD1V/hEPzZ2h8QTZ4jqnUk3TyzYPvFCIwIl+j3O95XXWZ7zMggBi+nkUVXFa7hTCZhRxdQdekmk9l/99G8H+F8PMaEflv0eH1Q5scyifSPTmpyHfqzqbJ430cka7IGOziPz1ixbDQYTIBL90qwqtl6D/IlwRK0WPJ52V9T2HKeJKSfjnfmVP8dRrbeSAYJUBS0wgNE8gsNGfeNjblbhKLYSmbGoFWXh2lg0LC7O9k91BnKvfeP+r8qiiwbdoEZ/OTG6iyFU4tyJRiVkyt7jMmURJZRuS1daKbtX0YYELj8919bAvbhznC419ogou85XEWZ4Jd/kJLpAdfuaujAZM3M893hUarto9wN1lwVHrCo26o5bxzg3jgmwt4fkT16/XSk2RIkleh40MB9uLDOcDtLKgCAe5a2nOd1kk5gKEyek1niR54yF/bKbCwdZz8zVeznv/w0yDSb9cBOYguf+YiwFL4Wt548onDAnoH7vTRYzK2pWQ== trevor@trevorsullivan.net"
}

data "aws_security_group" "default" {
    name = "default"
    vpc_id = "${aws_vpc.trevor.id}"
}

resource "aws_security_group_rule" "ssh" {
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 22
    to_port = 22
    description = "Allow SSH from anywhere"
    type = "ingress"
    protocol = "tcp"
    security_group_id = "${data.aws_security_group.default.id}"
}