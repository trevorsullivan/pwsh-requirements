#!/usr/bin/env bash

wget -O powershell.deb https://github.com/PowerShell/PowerShell/releases/download/v6.2.3/powershell_6.2.3-1.ubuntu.18.04_amd64.deb
sudo dpkg --install powershell.deb
sudo apt-get install --fix-broken --yes
