Project URL: https://gitlab.com/trevorsullivan/pwsh-requirements

# pwsh-requirements

A demonstration module for PowerShell Requirements

## Discussion Points

* Comparison with tools like Ansible
* Contrast with Desired State Configuration (DSC)
* Automate configuration of development environments
* Formatting output from Invoke-Requirement
* No need to develop and deploy custom modules
* Requirements are self-contained
* Depends-on