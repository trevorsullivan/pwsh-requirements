FROM codercom/code-server:v2

#RUN sudo apt-get install python3 python3-pip --yes

ADD ["install.sh", "install.ps1", "./"]

RUN ./install.sh
RUN pwsh -File ./install.ps1

ENTRYPOINT ["dumb-init", "code-server", "--host", "0.0.0.0", "--cert"]