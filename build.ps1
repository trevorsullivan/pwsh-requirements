#!/usr/bin/env pwsh

$Tag = 'pwsh-requirements'

# Build the container image
docker build --no-cache --tag $Tag $PSScriptRoot

# Run a new container
docker run --rm --interactive --tty --publish 8082:8080 $Tag