Install-Module -Name Requirements -Scope CurrentUser -MaximumVersion 2.2.7 -Force

$Requirements = @(
    @{
        Name = 'vscode-powershell'
        Describe = 'VSCode PowerShell Extension is installed'
        Test = { (code-server --list-extensions) -match 'ms\-vscode\.powershell' }
        Set = {
            $VSCodePowerShell = 'https://github.com/PowerShell/vscode-powershell/releases/download/v2019.9.0/PowerShell-2019.9.0.vsix'
            Invoke-WebRequest -OutFile ~/powershell.vsix -Uri $VSCodePowerShell
            code-server --install-extension ~/powershell.vsix
        }
    }
    @{
        Name = 'vscode-material-theme'
        Describe = 'VSCode Material Theme is installed'
        Test = { (code-server --list-extensions) -match '^equinusocio\.vsc-material-theme$' }
        Set = {
            code-server --install-extension equinusocio.vsc-material-theme
        }
    }
    @{
        Name = 'pwsh-polaris'
        Describe = 'Ensure Polaris module is installed'
        Test = { Get-Module -ListAvailable | Where-Object -FilterScript { $PSItem.Name -eq 'Polaris' } }
        Set = { Install-Module -Name Polaris -Scope CurrentUser -Force }
    }
    @{
        Name = 'vscode-material-icons'
        Describe = 'Ensure Visual Studio Code Material Icons are installed'
        Test = { (code-server --list-extensions) -match '^pkief\.material-icon-theme$' }
        Set = { code-server --install-extension pkief.material-icon-theme }
    }
)

$RequirementsResult = $Requirements | Invoke-Requirement
$RequirementsResult | Format-Checklist
$RequirementsResult | Format-Table
$RequirementsResult | Format-CallStack

@'
{
    "workbench.iconTheme": "material-icon-theme",
    "workbench.colorTheme": "Material Theme Palenight"
}
'@ | Out-File -FilePath $HOME/.local/share/code-server/User/settings.json

